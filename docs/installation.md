# Customer

## Clone the project

The PM project must be forked, then cloned.

### Prerequisites

have the repository forked and cloned.

`git clone https://github.com/jrnp97/customer.git`

### Installing

1. clonar el proyecto
2. bajar el proyecto
3. hacer el markdown installation.md usando algún editor de texto o este <https://pandao.github.io/editor.md/en.html>
4. abrir un IDE para ver la estructura del proyecto a nivel local
5. hacer una carpeta en la raíz del proyecto llamada docs
6. introducir el archivo
7. hacer un git add . para salvar los cambios
8. hacer un commit
9. subir los cambios al repo
10. inspeccionar en el remoto o página que los cambios se subieron

## Running the tests

the tests will be run after the `docker-compose up` and if they give error; fix them to continue

## Versioning

### 0.1

## Authors

Deiver Vazquez <dvazques@lsv-tech.com>

Zuledydis Barrios <zbarrios@lsv-tech.com>

Leandro Meza <lmeza@lsv-tech.com>

Jorge Zetien <jzetien@lsv-tech.com>

Luis Tilve <ltilve@lsv-tech.com>

## Link del PR

`git@gitlab.com:DeijoseDevelop/project-django-avanzado.git`
